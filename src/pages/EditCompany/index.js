import React, { useEffect, useState } from "react";
import { useParams } from "react-router";

import { toast } from "react-toastify";
import axios from "axios";

import CompanyForm from "../../components/CompanyForm";
import Spinner from "../../components/Spinner";

import validate from "../../util/validateInfo";
import _useForm from "../../components/CompanyForm/useForm";
import DOMPurify from "dompurify";

function EditCompanyForm() {
  const params = useParams();
  const useForm = _useForm(submitForm, validate);

  const [isLoading, setIsLoading] = useState(true);
  const [formLoading, setFormLoading] = useState(false);

  const getCompany = async () => {
    try {
      const { id } = params;
      const url = `http://localhost:5000/empresa/${id}`;
      const { data: c } = await axios.get(url);
      let banners = await Promise.all(
        c.banners.map(async (banner) => {
          const f = await axios.get(banner.url, {
            responseType: "blob",
          });

          return {
            file: f.data,
            data: banner,
          };
        })
      );

      banners = banners.map((banner) => {
        const { formato, titulo, updatedAt, id } = banner.data;

        const file = new File([banner.file], `${titulo}.${formato}`, {
          type: `image/${formato}`,
          lastModified: updatedAt,
        });

        file._idBanner = id;

        return file;
      });

      useForm.setValues({
        ...useForm.values,
        phone: c.telefone ?? "",
        name: c.nome ?? "",
        email: c.email ?? "",
        cep: c.cep ?? "",
        cnpj: c.cnpj ?? "",
        description: {
          plain: DOMPurify.sanitize(c.descricao),
          html: DOMPurify.sanitize(c.descricao) ?? "",
        },
        banners: {
          currentCompanyBannersId: c.banners.map((b) => b.id),
          removedBannersId: [],
          newBanners: [],
          files: banners,
        },
      });

      setIsLoading(false);
    } catch (ex) {
      toast.error(`Falha ao carregar empresa!`, {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }
  };

  useEffect(() => {
    getCompany();
  }, []);

  async function submitForm(e) {
    setIsLoading(true);
    setFormLoading(true);
    const { id } = params;

    try {
      const { name, cep, banners, cnpj, email, phone, description } =
        useForm.values;

      const formdata = new FormData();
      formdata.append("nome", name);
      formdata.append("email", email);
      formdata.append("cnpj", cnpj);
      formdata.append("cep", cep);
      formdata.append("telefone", phone);
      formdata.append("descricao", description.html);
      formdata.append("id_usuario", 1);

      if (banners.removedBannersId.length > 0) {
        formdata.append(
          "banners_removidos",
          JSON.stringify({ data: banners.removedBannersId })
        );
      }

      if (banners.newBanners.length > 0) {
        banners.newBanners.map((banner) => formdata.append("banners", banner));
      }

      await axios({
        method: "put",
        url: `http://localhost:5000/empresa/${id}`,
        data: formdata,
        headers: { "Content-Type": "multipart/form-data" },
      });

      toast.success("Empresa atualizada!", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });

      await getCompany();
    } catch (ex) {
      toast.error(`Falha ao editar empresa!`, {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    } finally {
      setFormLoading(false);
      setIsLoading(false);
    }
  }

  return (
    <>
      {isLoading ? (
        <Spinner />
      ) : (
        <CompanyForm
          useForm={useForm}
          isLoading={formLoading}
          title="Vamos editar sua empresa"
        />
      )}
    </>
  );
}

export default EditCompanyForm;
