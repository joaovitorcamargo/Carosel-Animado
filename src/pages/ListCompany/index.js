import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import axios from "axios";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPen } from "@fortawesome/free-solid-svg-icons";

import Spinner from "../../components/Spinner";

import S from "./styles.module.css";

function ListCompany() {
  const [companies, setCompanies] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const getCompanies = async () => {
      const url = "http://localhost:5000/empresa";
      const { data } = await axios.get(url);

      return data;
    };

    getCompanies()
      .then((c) => {
        setCompanies(c);
        setLoading(false);
      })
      .catch((err) => {
        toast.error(`Falha ao carregar empresas!`, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      });
  }, []);

  return (
    <main className={S.container}>
      <div className={S.content}>
        <div className={S.contentHeader}>
          <h1 className={S.contentTitle}>Lista de empresas</h1>
          <Link to="/cadastrar-empresa" className={S.createCompanyBtn}>
            Cadastrar <span className={S.createCompanyBtnIcon}>&#43;</span>
          </Link>
        </div>
        {loading ? (
          <Spinner />
        ) : (
          <ul className={S.list}>
            {companies.map(({ idempresa, nome, email }) => {
              return (
                <li key={nome} className={S.listItem}>
                  <Link className={S.itemLink} to={`/empresa/${idempresa}`}>
                    <div className={S.card}>
                      <div className={S.itemValue}>
                        <strong>nome: </strong>
                        <span> {nome}</span>
                      </div>
                      <div className={S.itemValue}>
                        <strong>e-mail: </strong>
                        <span> {email}</span>
                      </div>
                    </div>
                  </Link>
                  <div className={S.icon}>
                    <Link to={`/editar-empresa/${idempresa}`}>
                      <FontAwesomeIcon
                        className="icon-edit"
                        icon={faPen}
                      ></FontAwesomeIcon>
                    </Link>
                  </div>
                </li>
              );
            })}
          </ul>
        )}
      </div>
    </main>
  );
}

export default ListCompany;
