import React from "react";

import TransportDropdown from "../../components/TransportDropdown";

import S from "./styles.module.css";

const Transport = () => {
  return (
    <div className={S.container}>
      <form className={S.form}>
        <h2 className={S.title}>Transporte</h2>
        <div className={S.transportOptionContainer}>
          <label className={S.transportOptionTitle}>
            Clique e configure o transporte:
          </label>
          <TransportDropdown />
        </div>

        <button className={S.submitButton}>Confirmar</button>
      </form>
    </div>
  );
};

export default Transport;
