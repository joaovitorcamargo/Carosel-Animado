import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import DOMPurify from "dompurify";

import { toast } from "react-toastify";
import axios from "axios";

import Slider from "../../components/Slider";
import Spinner from "../../components/Spinner";

import S from "./styles.module.css";

function SliderPage() {
  const [company, setCompany] = useState([]);
  const [loading, setLoading] = useState(true);

  const params = useParams();

  useEffect(() => {
    const getCompany = async () => {
      const { id } = params;
      const url = `http://localhost:5000/empresa/${id}`;
      const { data } = await axios.get(url);

      return data;
    };

    getCompany()
      .then((c) => {
        setCompany({
          ...c,
          descricao: DOMPurify.sanitize(c.descricao),
        });

        setLoading(false);
      })
      .catch((err) => {
        toast.error(`Falha ao carregar empresa!`, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      });
  }, []);

  return (
    <main className={S.container}>
      {loading ? (
        <Spinner />
      ) : (
        <div className={S.content}>
          <div className={S.contentHeader}>
            <h1 className={S.title}>{company.nome}</h1>
            <Slider images={company.banners} />
          </div>
          <div className={S.contentDescription}>
            <h2 className={S.descriptionTitle}>Descrição</h2>
            <div className={S.divider}></div>
            <div
              className={S.description}
              dangerouslySetInnerHTML={{ __html: company.descricao }}
            ></div>
          </div>
        </div>
      )}
    </main>
  );
}

export default SliderPage;
