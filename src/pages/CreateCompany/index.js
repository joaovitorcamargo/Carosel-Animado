import React, { useState } from "react";

import { toast } from "react-toastify";
import axios from "axios";

import CompanyForm from "../../components/CompanyForm";

import validate from "../../util/validateInfo";
import _useForm from "../../components/CompanyForm/useForm";

function CreateCompanyForm() {
  const useForm = _useForm(submitForm, validate);

  const [isLoading, setIsLoading] = useState(false);

  async function submitForm(e) {
    setIsLoading(true);

    try {
      const { name, cep, banners, cnpj, email, phone, description } =
        useForm.values;

      const formdata = new FormData();
      formdata.append("nome", name);
      formdata.append("email", email);
      formdata.append("cnpj", cnpj);
      formdata.append("cep", cep);
      formdata.append("telefone", phone);
      formdata.append("descricao", description.html);
      formdata.append("id_usuario", 1);

      banners.files.map((banner) => formdata.append("banners", banner.file));

      await axios({
        method: "post",
        url: "http://localhost:5000/empresa",
        data: formdata,
        headers: { "Content-Type": "multipart/form-data" },
      });

      toast.success("Empresa cadastrada!", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    } catch (ex) {
      toast.error(`Falha ao cadastrar empresa!`, {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    } finally {
      setIsLoading(false);
    }
  }

  return <CompanyForm useForm={useForm} isLoading={isLoading} />;
}

export default CreateCompanyForm;
