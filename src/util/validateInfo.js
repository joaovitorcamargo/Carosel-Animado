export default function validateInfo(values) {
  let errors = {};

  if (!values.name.trim()) {
    errors.name = "Digite o nome";
  }

  //Email
  if (!values.email) {
    errors.email = "Digite o email";
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)) {
    errors.email = "Email invalido";
  }

  if (!values.cnpj) {
    errors.cnpj = "Digite o CNPJ";
  } else if (values.cnpj.length < 14) {
    errors.cnpj = "CNPJ deve ter 14 dígitos ou mais";
  }

  if (!values.phone) {
    errors.phone = "Digite o telefone";
  } else if (values.phone.length < 9) {
    errors.phone = "O telefone deve ter ao menos 9 digitos";
  }

  if (!values.cep) {
    errors.cep = "Digite o cep";
  } else if (!values.cep.length === 5) {
    errors.cep = "O cep deve ter 5 digitos";
  }

  if (!values.description.plain) {
    errors.description = "Digite uma descrição";
  } else if (values.description.plain.length < 5) {
    errors.description = "A descrição deve ter ao menos 5 caracteres";
  }

  return errors;
}
