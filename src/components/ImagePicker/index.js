import React, { useRef } from "react";
import Dropzone from "react-dropzone-uploader";
import "react-dropzone-uploader/dist/styles.css";

import S from "./styles.module.css";

const ImagePicker = ({ onChange, initialFiles }) => {
  const submitRef = useRef();

  const handleChangeStatus = ({ meta }, status) => {
    if (submitRef.current) {
      let files = submitRef.current.files;

      if (status === "removed") {
        files = files.filter(({ meta: m }) => meta.id !== m.id);
      }

      onChange(files);
    }
  };

  return (
    <Dropzone
      ref={submitRef}
      addClassNames={{
        inputLabel: S.inputLabel,
        dropzone: S.dropzone,
        preview: S.preview,
        inputLabelWithFiles: S.inputLabelWithFiles,
      }}
      onChangeStatus={handleChangeStatus}
      submitButtonContent={null}
      SubmitButtonComponent={null}
      initialFiles={initialFiles}
      inputWithFilesContent="Clique aqui ou arraste para inserir mais banners"
      inputContent="Clique ou arraste para inserir os banners da empresa"
      accept="image/*"
    />
  );
};

export default ImagePicker;
