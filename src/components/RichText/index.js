import React, { useState } from "react";
import { EditorState, ContentState, convertFromHTML } from "draft-js";
import { Editor } from "react-draft-wysiwyg";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";

import S from "./styles.module.css";

function TextEditor({ onChange, initialHtml }) {
  const state = initialHtml
    ? EditorState.createWithContent(
        ContentState.createFromBlockArray(convertFromHTML(initialHtml))
      )
    : EditorState.createEmpty();

  const [editorState, setEditorState] = useState(state);

  const onEditorStateChange = (editorState) => {
    setEditorState(editorState);
    onChange(editorState);
  };

  return (
    <div>
      <Editor
        editorState={editorState}
        toolbarClassName={S.toolBar}
        wrapperClassName={S.wrapper}
        editorClassName={S.editor}
        onEditorStateChange={onEditorStateChange}
      />
    </div>
  );
}

export default TextEditor;
