import React from "react";

import S from "./styles.module.css";

const DELAY = 2500;

function Slider({ images }) {
  const [index, setIndex] = React.useState(0);
  const timeoutRef = React.useRef(null);

  function resetTimeout() {
    if (timeoutRef.current) {
      clearTimeout(timeoutRef.current);
    }
  }

  React.useEffect(() => {
    resetTimeout();
    timeoutRef.current = setTimeout(
      () =>
        setIndex((prevIndex) =>
          prevIndex === images.length - 1 ? 0 : prevIndex + 1
        ),
      DELAY
    );

    return () => {
      resetTimeout();
    };
  }, [index]);

  return (
    <div className={S.slideshow}>
      <div
        className={S.slideshowSlider}
        style={{ transform: `translate3d(${-index * 100}%, 0, 0)` }}
      >
        {images.map(({ url, titulo }, i) => (
          <img className={S.slide} key={i} alt={titulo} src={url} />
        ))}
      </div>

      <div className={S.slideshowDots}>
        {images.map((_, idx) => (
          <div
            key={idx}
            className={`${S.slideshowDot}${
              index === idx ? ` ${S.slideshowDotActive}` : ""
            }`}
            onClick={() => {
              setIndex(idx);
            }}
          ></div>
        ))}
      </div>
    </div>
  );
}

export default Slider;
