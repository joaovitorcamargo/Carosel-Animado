import React from "react";

import FormBody from "./components/FormBody";
import "./styles.css";
import TextEditor from "../RichText";
import ImagePicker from "../ImagePicker";

const Form = ({
  useForm,
  isLoading,
  title = "Vamos cadastrar sua empresa...",
}) => {
  return (
    <form onSubmit={useForm.handleSubmit} className="form-wrapper">
      <div className="form-container">
        <FormBody title={title} useForm={useForm} isLoading={isLoading} />
        <div className="form-content-right">
          <div className="form-inputs">
            <h3 className="description-title">Descrição da empresa</h3>

            <TextEditor
              id="description"
              name="description"
              initialHtml={useForm.values.description.html}
              onChange={useForm.handleDescriptionChange}
            />

            {useForm.errors.description && <p>{useForm.errors.description}</p>}
          </div>

          <div className="form-inputs">
            <ImagePicker
              id="banners"
              name="banners"
              initialFiles={useForm.values.banners.files}
              onChange={useForm.handleBannersChange}
            />
            {useForm.errors.banners && <p>{useForm.errors.banners}</p>}
          </div>
        </div>
      </div>
    </form>
  );
};

export default Form;
