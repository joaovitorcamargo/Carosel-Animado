import { useState, useEffect } from "react";

import { stateToHTML } from "draft-js-export-html";

import DOMPurify from "dompurify";

const useForm = (callback, validate) => {
  const [values, setValues] = useState({
    name: "",
    email: "",
    cnpj: "",
    phone: "",
    cep: "",
    description: {
      html: "",
      plain: "",
    },
    banners: {
      currentCompanyBannersId: [],
      removedBannersId: [],
      newBanners: [],
      files: [],
    },
  });
  const [errors, setErrors] = useState({});

  const [isSubmitting, setIsSubmitting] = useState(false);

  const handleInputChange = (e) => {
    const { name, value } = e.target;

    setValues({
      ...values,
      [name]: value,
    });
  };

  const handleDescriptionChange = (editorState) => {
    setValues({
      ...values,
      description: {
        plain: editorState.getCurrentContent().getPlainText(),
        html: DOMPurify.sanitize(stateToHTML(editorState.getCurrentContent())),
      },
    });
  };

  const handleBannersChange = (files = []) => {
    if (!files) return;

    const newBanners = [];
    const currentBannersId = [];

    files.map(({ file }) => {
      if (!file._idBanner) {
        return newBanners.push(file);
      }

      if (!currentBannersId.includes(file._idBanner)) {
        return currentBannersId.push(file._idBanner);
      }
    });

    const removedBannersId = values.banners.currentCompanyBannersId.filter(
      (id) => !currentBannersId.includes(id)
    );

    setValues({
      ...values,
      banners: {
        ...values.banners,
        removedBannersId,
        newBanners,
        files,
      },
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    setErrors(validate(values));
    setIsSubmitting(true);
  };

  useEffect(() => {
    if (Object.keys(errors).length === 0 && isSubmitting) {
      callback();
    }
  }, [errors]);

  return {
    handleInputChange,
    handleDescriptionChange,
    handleBannersChange,
    handleSubmit,
    setValues,
    values,
    errors,
  };
};

export default useForm;
