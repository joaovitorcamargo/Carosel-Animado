import React from "react";
import { Link } from "react-router-dom";

const FormBody = ({ useForm, isLoading, title }) => {
  const { handleInputChange, values, errors } = useForm;

  return (
    <div className="form-content-left">
      <div className="form">
        <h1>{title}</h1>
        <div className="form-inputs">
          <label htmlFor="name" className="form-label">
            Nome
          </label>
          <input
            id="name"
            type="text"
            name="name"
            className="form-input"
            placeholder="Digite o nome da Empresa"
            value={values.name}
            onChange={handleInputChange}
          />
          {errors.name && <p>{errors.name}</p>}
        </div>

        <div className="form-inputs">
          <label htmlFor="email" className="form-label">
            Email
          </label>
          <input
            id="email"
            type="email"
            name="email"
            className="form-input"
            placeholder="Digite o email"
            value={values.email}
            onChange={handleInputChange}
          />
          {errors.email && <p>{errors.email}</p>}
        </div>

        <div className="form-inputs">
          <label htmlFor="cnpj" className="form-label">
            CNPJ
          </label>
          <input
            id="cnpj"
            name="cnpj"
            className="form-input"
            placeholder="Digite o CNPJ"
            value={values.cnpj}
            onChange={handleInputChange}
          />
          {errors.cnpj && <p>{errors.cnpj}</p>}
        </div>

        <div className="form-inputs">
          <label htmlFor="phone" className="form-label">
            Telefone
          </label>
          <input
            id="phone"
            name="phone"
            className="form-input"
            placeholder="Digite o telefone"
            value={values.phone}
            onChange={handleInputChange}
          />
          {errors.phone && <p>{errors.phone}</p>}
        </div>

        <div className="form-inputs">
          <label htmlFor="cep" className="form-label">
            CEP
          </label>
          <input
            id="cep"
            name="cep"
            className="form-input"
            placeholder="Digite o CEP"
            value={values.cep}
            onChange={handleInputChange}
          />
          {errors.cep && <p>{errors.cep}</p>}
        </div>

        <button className="form-input-btn" type="submit">
          {isLoading ? "Aguarde..." : "Salvar"}
        </button>
        <span className="form-input-login">
          Já possui uma empresa cadastrada e gostaria de editá-la?{" "}
          <Link to="/">Clique aqui</Link>
        </span>
      </div>
    </div>
  );
};

export default FormBody;
