import { ReactComponent as CaretIcon } from "../../assets/images/caret.svg";
import { ReactComponent as ChevronIcon } from "../../assets/images/chevron.svg";
import { ReactComponent as ArrowIcon } from "../../assets/images/arrow.svg";

import "./styles.css";

import React, { useState, useEffect, useRef } from "react";
import { CSSTransition } from "react-transition-group";

function TransportDropdown() {
  return (
    <NavItem icon={<CaretIcon />}>
      <DropdownMenu></DropdownMenu>
    </NavItem>
  );
}

function NavItem(props) {
  const [open, setOpen] = useState(false);

  return (
    <div className="open-dropdown">
      <a href="#dsdsd" className="icon-button" onClick={() => setOpen(!open)}>
        {props.icon}
      </a>

      {open && props.children}
    </div>
  );
}

function DropdownMenu() {
  const [activeMenu, setActiveMenu] = useState("main");

  const dropdownRef = useRef(null);

  function DropdownItem(props) {
    return (
      <a
        href="#example"
        className="menu-item"
        onClick={() => props.goToMenu && setActiveMenu(props.goToMenu)}
      >
        <span className="icon-button">{props.leftIcon}</span>
        {props.children}
        <span className="icon-right">{props.rightIcon}</span>
      </a>
    );
  }

  return (
    <div className="dropdown" ref={dropdownRef}>
      <CSSTransition
        in={activeMenu === "main"}
        timeout={500}
        classNames="menu-primary"
        unmountOnExit
      >
        <div className="menu">
        
          <DropdownItem
            leftIcon="📍"
            rightIcon={<ChevronIcon />}
            goToMenu="settings"
          >
            Origem
          </DropdownItem>
          <DropdownItem
            leftIcon="📍"
            rightIcon={<ChevronIcon />}
            goToMenu="animals"
          >
            Destino
          </DropdownItem>
          <DropdownItem leftIcon="🚚">Frete</DropdownItem>
        </div>
      </CSSTransition>

      <CSSTransition
        in={activeMenu === "settings"}
        timeout={500}
        classNames="menu-secondary"
        unmountOnExit
      >
        <div className="menu">
          <DropdownItem goToMenu="main" leftIcon={<ArrowIcon />}>
            <h2>Origem</h2>
          </DropdownItem>
          <DropdownItem leftIcon="📍">Goiânia</DropdownItem>
          <DropdownItem leftIcon="📍">Mato Grosso</DropdownItem>
          <DropdownItem leftIcon="📍">Florianópolis</DropdownItem>
          <DropdownItem leftIcon="📍">Bahia</DropdownItem>
        </div>
      </CSSTransition>

      <CSSTransition
        in={activeMenu === "animals"}
        timeout={500}
        classNames="menu-secondary"
        unmountOnExit
      >
        <div className="menu">
          <DropdownItem goToMenu="main" leftIcon={<ArrowIcon />}>
            <h2>Destino</h2>
          </DropdownItem>
          <DropdownItem leftIcon="📍">São Paulo</DropdownItem>
          <DropdownItem leftIcon="📍">Rio de Janeiro</DropdownItem>
          <DropdownItem leftIcon="📍">João Pessoa</DropdownItem>
          <DropdownItem leftIcon="📍">Acre</DropdownItem>
        </div>
      </CSSTransition>
    </div>
  );
}

export default TransportDropdown;
