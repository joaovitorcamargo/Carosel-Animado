import React from "react";

import { BrowserRouter, Route, Routes as Router } from "react-router-dom";

import ListCompany from "./pages/ListCompany";
import CreateCompany from "./pages/CreateCompany";
import SliderPage from "./pages/SliderPage";
import EditCompany from "./pages/EditCompany";
import Transport from "./pages/Transport";

const Routes = () => {
  return (
    <BrowserRouter>
      <Router>
        <Route path="*" element={<ListCompany />} />
        <Route path="/empresa/:id" element={<SliderPage />} />
        <Route path="/cadastrar-empresa/" element={<CreateCompany />} />
        <Route path="/editar-empresa/:id" element={<EditCompany />} />
        <Route path="/transporte" element={<Transport />} />
      </Router>
    </BrowserRouter>
  );
};

export default Routes;
